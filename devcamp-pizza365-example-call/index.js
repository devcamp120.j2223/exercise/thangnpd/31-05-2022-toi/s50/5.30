const express = require('express');
const app = express();
const port = 8000;
const path = require('path');

app.get("/", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/sample.07restAPI.order.pizza365.v3.2.0.html`));
})
 
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})
